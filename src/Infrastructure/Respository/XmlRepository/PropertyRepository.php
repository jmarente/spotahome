<?php

namespace JMarente\Spotahome\Infrastructure\Respository\XmlRepository;

use JMarente\Spotahome\Domain\Entity\Property\Property;
use JMarente\Spotahome\Domain\Entity\Property\PropertyRepositoryInterface;

Class PropertyRepository implements PropertyRepositoryInterface
{

    const DEFAULT_ORDER_FIELD = 'id';
    const DEFAULT_ORDER_DIRECTION = 'asc';

    const ORDER_FIELDS = ['id', 'title', 'link', 'city', 'image'];
    const ORDER_DIRECTIONS = ['asc', 'desc'];

    public function getList(string $orderField = self::DEFAULT_ORDER_FIELD, string $orderDirection = self::DEFAULT_ORDER_DIRECTION): array {
        $properties= $this->fetch();
        return $this->sort($properties, $orderField, $orderDirection);
    }

    private function fetch() {

        $propertiesXML = new \SimpleXMLElement(getenv('PROPERTIES_XML'), 0 , true);
        return $this->convert($propertiesXML);
    }

    private function convert(\SimpleXMLElement $data): array {
        $properties = [];
        foreach($data->ad as $element) {
            $properties[] = new Property(
                (int) $element->id,
                (string) $element->title,
                (string) $element->url,
                (string) $element->city,
                (string) $element->pictures[0]->picture->picture_url
            );
        }
        return $properties;
    }

    private function sort(array $properties, string $orderField, string $orderDirection) {
        $orderField = strtolower($orderField);
        $orderDirection = strtolower($orderDirection);

        $_orderField = in_array($orderField, self::ORDER_FIELDS) ? $orderField : self::DEFAULT_ORDER_FIELD;
        $_orderDirection = in_array($orderDirection, self::ORDER_DIRECTIONS) ? $orderDirection : self::DEFAULT_ORDER_DIRECTION;

        $sortFunction = 'sortBy' . ucfirst($_orderField) . ucfirst($_orderDirection);

        usort($properties, array($this, $sortFunction));

        return $properties;
    }

    private function sortByIdAsc(property $p1, property $p2) {
        return $p1->getId() >= $p2->getId();
    }

    private function sortByIdDesc(property $p1, property $p2) {
        return $p1->getId() < $p2->getId();
    }

    private function sortByTitleAsc(property $p1, property $p2) {
        return $p1->getTitle() >= $p2->getTitle();
    }

    private function sortByTitleDesc(property $p1, property $p2) {
        return $p1->getTitle() < $p2->getTitle();
    }

    private function sortByLinkAsc(property $p1, property $p2) {
        return $p1->getLink() >= $p2->getLink();
    }

    private function sortByLinkDesc(property $p1, property $p2) {
        return $p1->getLink() < $p2->getLink();
    }

    private function sortByCityAsc(property $p1, property $p2) {
        return $p1->getCity() >= $p2->getCity();
    }

    private function sortByCityDesc(property $p1, property $p2) {
        return $p1->getCity() < $p2->getCity();
    }

    private function sortByImageAsc(property $p1, property $p2) {
        return $p1->getImage() >= $p2->getImage();
    }

    private function sortByImageDesc(property $p1, property $p2) {
        return $p1->getImage() < $p2->getImage();
    }

}
