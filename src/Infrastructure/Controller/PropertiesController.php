<?php

namespace JMarente\Spotahome\Infrastructure\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use JMarente\Spotahome\Application\Query\ListPropertiesQuery;

class PropertiesController extends AbstractController
{

    private $listPropertiesQuery;

    public function __construct(ListPropertiesQuery $listPropertiesQuery) {
        $this->listPropertiesQuery = $listPropertiesQuery;
    }

    public function index() {
        return $this->redirectToRoute('properties_list');
    }

    public function list($orderField = 'id', $orderDirection = 'asc') {

        $properties = $this->listPropertiesQuery->execute($orderField, $orderDirection);

        return $this->render('property/list.html.twig', [
            'properties'     => $properties,
            'orderField'     => $orderField,
            'orderDirection' => $orderDirection,
            'headers'        => ['Id', 'Title', 'Link', 'City', 'Image'],
        ]);
    }

    public function download($orderField = 'id', $orderDirection= 'asc') {

        $properties = $this->listPropertiesQuery->execute($orderField, $orderDirection);

        return $this->json($properties,
            Response::HTTP_OK,
            [
                'Content-Disposition' => "attachment; filename=\"properties_${orderField}_${orderDirection}.json\"",
            ]
        );
    }

}
