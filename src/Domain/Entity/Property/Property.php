<?php

namespace JMarente\Spotahome\Domain\Entity\Property;

class Property implements \JsonSerializable
{

    private $id;
    private $title;
    private $link;
    private $city;
    private $image;

    function __construct(int $id, string $title, string $link, string $city, string $image) {
        $this->id = $id;
        $this->title = $title;
        $this->link = $link;
        $this->city = $city;
        $this->image = $image;
    }

    public function jsonSerialize () {
        return [
            'id'    => $this->id,
            'link'  => $this->link,
            'title' => $this->title,
            'city'  => $this->city,
            'image' => $this->image,
        ];
    }

    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getLink() {
        return $this->link;
    }

    public function getCity() {
        return $this->city;
    }

    public function getImage() {
        return $this->image;
    }
}
