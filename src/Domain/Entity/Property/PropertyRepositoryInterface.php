<?php

namespace JMarente\Spotahome\Domain\Entity\Property;

interface PropertyRepositoryInterface
{

    public function getList(string $orderField = 'id', string $orderDirection = 'asc'): array;

}

