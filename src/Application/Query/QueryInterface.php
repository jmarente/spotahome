<?php

namespace JMarente\Spotahome\Application\Query;

interface QueryInterface
{
    public function execute();
}
