<?php

namespace JMarente\Spotahome\Application\Query;

use JMarente\Spotahome\Domain\Entity\Property\PropertyRepositoryInterface;

class ListPropertiesQuery implements QueryInterface
{

    private $propertiesRepository;

    public function __construct(PropertyRepositoryInterface $propertiesRepository) {
        $this->propertiesRepository = $propertiesRepository;
    }

    public function execute(string $orderField = 'id', string $orderDirection = 'asc') {
        return $this->propertiesRepository->getList($orderField, $orderDirection);
    }

}

