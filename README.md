# Spotahome test

Write a web application that fetches an XML from a remote url and presents the data in a
sortable table.

# Requirements

Install [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/).

# Setup

**Important**: make sure port 8080 is not being used in your system

    docker-compose up --build -d
    docker exec -i -t spotahome-php-fpm composer install

Go to http://localhost:8080

To clean docker containers:

    docker-compose down

# Tests

    docker exec -i -t spotahome-php-fpm phpunit tests
